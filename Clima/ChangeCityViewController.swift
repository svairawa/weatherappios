//
//  ChangeCityViewController.swift
//  WeatherApp
//
//  Created by Shanya on 02/10/2018.
//  Copyright (c) 2018 Shanya. All rights reserved.
//

import UIKit


//The protocol declaration
protocol ChangeCityDelegate {
    func userEnteredANewCityName(city: String)
}


class ChangeCityViewController: UIViewController {
    
    //Declaration of the delegate variable
    var delegate : ChangeCityDelegate?
    
    @IBOutlet weak var changeCityTextField: UITextField!


    @IBAction func getWeatherPressed(_ sender: AnyObject) {
        
        
        
        //Get the city name
        let cityName = changeCityTextField.text!
        
        //Call the method userEnteredANewCityName
        delegate?.userEnteredANewCityName(city: cityName)
        
        //Go back to the WeatherViewController
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
